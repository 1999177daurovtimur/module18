﻿#include <iostream>
#include <string>
using namespace std;

class PlayerX
{
public:
    string name;
    int points;

    PlayerX()
    {
        name = "----- ";
        points = 0;
    }

    PlayerX(string _name, int _points)
    {
        name = _name;
        points = _points;
    }

    void Show()
    {
        cout << "Player name: " << name << "\t\t" << "Points: " << points << endl;
    }
};

    int main()
    {
        int size = 0;
        cout << endl;
        cout << "Enter number of players" << endl; cout << endl;
        cout << "Number of players = "; cin >> size;
        cout << "=========================================="; cout << endl; cout << endl;

        PlayerX* arr = new PlayerX[size];

        for (int i = 0; i != size; i++)
        {
            string playerName;
            cout << "Enter player#" << i + 1 << " name: ";
            cin >> playerName;
            
            int playerPoints;
            cout << "Enter player#" << i + i << " score: ";
            cin >> playerPoints;
            cout << endl; 
            cout << "=========================================="; cout << endl;

            arr[i].name = playerName;
            arr[i].points = playerPoints;
        }
        
        for (int turn = 0; turn < size; turn++)
        {
            bool flag = true;
            for (int i = 0; i < size - 1; i++)
            {
                if (arr[i].points > arr[i + 1].points)
                {
                    flag = false;
                  swap(arr[i].points, arr[i + 1].points);
                }
            }
            if (flag)
            {
                break;
            }
        }
      
        for (int i = 0; i < size; i++)
        {
            arr[i].Show();
        }
        delete[] arr;

        return 0;

    }